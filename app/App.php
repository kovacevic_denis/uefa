<?php

namespace App;

use App\League\LeagueInterface;
use Symfony\Component\DependencyInjection\Container;

class App
{
    /** @var Container */
    protected $container;

    /** @var  LeagueInterface */
    protected $league;

    public function __construct(Container $container, LeagueInterface $league)
    {
        $this->container = $container;
        $this->league = $league;
    }

    public function play()
    {
        $this->league->play();
    }

    public function getResults()
    {
        return $this->league->getStatistics();
    }
}