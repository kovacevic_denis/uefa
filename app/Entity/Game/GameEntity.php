<?php
namespace App\Entity\Game;

use App\Entity\Team\FullTeamEntity;

/**
 * Class GameEntity
 * @package App\Entity\Game
 */
class GameEntity
{
    /** @var  FullTeamEntity */
    protected $home;

    /** @var  FullTeamEntity */
    protected $opponent;

    /** @var  string */
    protected $result;

    /**
     * @return FullTeamEntity
     */
    public function getHome(): FullTeamEntity
    {
        return $this->home;
    }

    /**
     * @param FullTeamEntity $home
     */
    public function setHome(FullTeamEntity $home)
    {
        $this->home = $home;
    }

    /**
     * @return FullTeamEntity
     */
    public function getOpponent(): FullTeamEntity
    {
        return $this->opponent;
    }

    /**
     * @param FullTeamEntity $opponent
     */
    public function setOpponent(FullTeamEntity $opponent)
    {
        $this->opponent = $opponent;
    }

    /**
     * @return string
     */
    public function getResult(): string
    {
        return $this->result;
    }

    /**
     * @param string $result
     */
    public function setResult(string $result)
    {
        $this->result = $result;
    }
}