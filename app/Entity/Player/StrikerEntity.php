<?php

namespace App\Entity\Player;

/**
 * Class Striker
 * @package App\Entity
 */
class StrikerEntity extends AbstractPlayerEntity
{
    /** @var  string */
    protected $position = self::STRIKER;
}