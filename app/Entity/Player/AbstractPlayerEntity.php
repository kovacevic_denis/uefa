<?php

namespace App\Entity\Player;

use App\Entity\AbstractEntity;

/**
 * Class AbstractPlayer
 * @package App\Entity
 */
abstract class AbstractPlayerEntity extends AbstractEntity
{
    const DEFENDER = 'defender';
    const GOALKEEPER = 'goalkeeper';
    const MIDFIELDER = 'midfielder';
    const STRIKER = 'striker';

    const MIN = 30;
    const MIN_STRONG = 80;
    const MIN_MIDDLE = 66;
    const MAX_MIDDLE = 79;
    const MAX = 100;
    const MAX_WEAK = 65;

    const AVAILABLE_POSITIONS = [
        self::DEFENDER,
        self::GOALKEEPER,
        self::MIDFIELDER,
        self::STRIKER,
    ];

    /** @var  string */
    protected $position;

    /** @var  int */
    protected $quality;

    /** @var  int */
    protected $speed;

    /**
     * AbstractPlayer constructor.
     * @param $quality
     * @param $speed
     */
    public function __construct(int $quality, int $speed)
    {
        $this->quality = $quality;
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     */
    public function setSpeed(int $speed)
    {
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality(int $quality)
    {
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function getPosition(): string
    {
        return $this->position;
    }

    /**
     * @param string $position
     */
    public function setPosition(string $position)
    {
        $this->position = $position;
    }
}