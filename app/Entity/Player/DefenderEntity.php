<?php

namespace App\Entity\Player;

/**
 * Class Defender
 * @package App\Entity
 */
class DefenderEntity extends AbstractPlayerEntity
{
    /** @var  string */
    protected $position = self::DEFENDER;
}