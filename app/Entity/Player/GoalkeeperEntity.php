<?php

namespace App\Entity\Player;

/**
 * Class Goalkeeper
 * @package App\Entity
 */
class GoalkeeperEntity extends AbstractPlayerEntity
{
    /** @var  string */
    protected $position = self::GOALKEEPER;
}