<?php

namespace App\Entity\Player;

/**
 * Class Midfielder
 * @package App\Entity
 */
class MidfielderEntity extends AbstractPlayerEntity
{
    /** @var  string */
    protected $position = self::MIDFIELDER;
}