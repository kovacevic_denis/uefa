<?php

namespace App\Entity\Team;

use App\Entity\Player\AbstractPlayerEntity;

/**
 * Class FullTeamEntity
 * @package App\Entity\Team
 */
class FullTeamEntity extends AbstractTeamEntity
{
    /** @var  StarterTeamEntity */
    protected $starterTeamEntity;

    /** @var  AbstractPlayerEntity[] */
    protected $injuredPlayers;

    /**
     * @return StarterTeamEntity
     */
    public function getStarterTeamEntity()
    {
        return $this->starterTeamEntity;
    }

    /**
     * @param StarterTeamEntity $starterTeamEntity
     */
    public function setStarterTeamEntity(StarterTeamEntity $starterTeamEntity)
    {
        $this->starterTeamEntity = $starterTeamEntity;
    }

    public function addInjuredPlayer(AbstractPlayerEntity $playerEntity)
    {
        $this->injuredPlayers[] = $playerEntity;
    }

    /**
     * @return AbstractPlayerEntity[]
     */
    public function getInjuredPlayers(): array
    {
        return $this->injuredPlayers;
    }
}