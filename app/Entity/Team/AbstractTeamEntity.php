<?php

namespace App\Entity\Team;

use App\Entity\Player\AbstractPlayerEntity;
use App\Entity\Player\DefenderEntity;
use App\Entity\Player\GoalkeeperEntity;
use App\Entity\Player\MidfielderEntity;
use App\Entity\Player\StrikerEntity;
use App\Exceptions\PlayerPositionNotExistException;

/**
 * Class AbstractTeamEntity
 * @package App\Entity
 */
abstract class AbstractTeamEntity
{
    const WEAK = 'weak';
    const STRONG = 'strong';
    const MIDDLE = 'middle';
    const EQUAL = 'equal';

    /** @var  GoalkeeperEntity[] */
    protected $goalkeepers;

    /** @var  DefenderEntity[] */
    protected $defenders;

    /** @var  MidfielderEntity[] */
    protected $midfielders;

    /** @var  StrikerEntity[] */
    protected $strikers;

    public function addPlayer(AbstractPlayerEntity $abstractPlayer)
    {
        $class = get_class($abstractPlayer);

        switch ($class) {
            case GoalkeeperEntity::class:
                $this->goalkeepers[] = $abstractPlayer;
                $this->order($this->goalkeepers, 'orderPlayersByQuality');
                break;
            case DefenderEntity::class:
                $this->defenders[] = $abstractPlayer;
                $this->order($this->defenders, 'orderPlayersByQuality');
                break;
            case MidfielderEntity::class:
                $this->midfielders[] = $abstractPlayer;
                $this->order($this->midfielders, 'orderPlayersByQuality');
                break;
            case StrikerEntity::class:
                $this->strikers[] = $abstractPlayer;
                $this->order($this->strikers, 'orderPlayersByQuality');
                break;
            default:
                throw new PlayerPositionNotExistException();
                break;
        }
    }

    /**
     * @param array $playersByPosition
     */
    public function addPlayersByPosition(array $playersByPosition)
    {
        foreach ($playersByPosition as $players) {
            foreach ($players as $player) {
                $this->addPlayer($player);
            }
        }
    }

    /**
     * @param AbstractPlayerEntity[] $players
     */
    public function addPlayers(array $players)
    {
        foreach ($players as $player) {
            $this->addPlayer($player);
        }
    }

    /**
     * @return GoalkeeperEntity[]
     */
    public function getGoalkeepers(): array
    {
        return $this->goalkeepers;
    }

    /**
     * @param GoalkeeperEntity[] $goalkeepers
     */
    public function setGoalkeepers(array $goalkeepers)
    {
        $this->goalkeepers = $goalkeepers;
    }

    /**
     * @return DefenderEntity[]
     */
    public function getDefenders(): array
    {
        return $this->defenders;
    }

    /**
     * @param DefenderEntity[] $defenders
     */
    public function setDefenders(array $defenders)
    {
        $this->defenders = $defenders;
    }

    /**
     * @return MidfielderEntity[]
     */
    public function getMidfielders(): array
    {
        return $this->midfielders;
    }

    /**
     * @param MidfielderEntity[] $midfielders
     */
    public function setMidfielders(array $midfielders)
    {
        $this->midfielders = $midfielders;
    }

    /**
     * @return StrikerEntity[]
     */
    public function getStrikers(): array
    {
        return $this->strikers;
    }

    /**
     * @param StrikerEntity[] $strikers
     */
    public function setStrikers(array $strikers)
    {
        $this->strikers = $strikers;
    }

    /**
     * @return AbstractPlayerEntity[]
     */
    public function getAllPlayers()
    {
        return array_merge(
            $this->getGoalkeepers(),
            $this->getDefenders(),
            $this->getMidfielders(),
            $this->getStrikers()
        );
    }

    /**
     * @param string $position
     * @param string $orderBy
     * @throws PlayerPositionNotExistException
     */
    public function playersOrderByPosition(string $position, string $orderBy)
    {
        switch ($position) {
            case AbstractPlayerEntity::DEFENDER:
                $this->order($this->defenders, $orderBy);
                break;
            case AbstractPlayerEntity::MIDFIELDER:
                $this->order($this->midfielders, $orderBy);
                break;
            case AbstractPlayerEntity::STRIKER:
                $this->order($this->strikers, $orderBy);
                break;
            default:
                throw new PlayerPositionNotExistException();
                break;
        }
    }

    public function order(&$players, $orderBy)
    {
        usort($players, $orderBy);
    }

    public function getScore()
    {
        $players = $this->getAllPlayers();
        $playersCount = count($players);

        $qualitySum = 0;
        $speedSum = 0;

        foreach ($players as $player) {
            $qualitySum += $player->getQuality();
            $speedSum += $player->getSpeed();
        }

        $qualityAvg = $qualitySum / $playersCount;
        $speedAvg = $speedSum / $playersCount;

        return round(($qualityAvg + $speedAvg) / 2);
    }

    public function getLevel()
    {
        $level = self::MIDDLE;

        if ($this->getScore() <= AbstractPlayerEntity::MAX_WEAK) {
            $level = self::WEAK;
        } elseif ($this->getScore() >= AbstractPlayerEntity::MIN_STRONG) {
            $level = self::STRONG;
        }

        return $level;
    }

    public function spliceTopPlayerByPosition($position)
    {
        switch ($position) {
            case AbstractPlayerEntity::DEFENDER:
                return array_splice($this->defenders, 0, 1)[0] ?? null;
                break;
            case AbstractPlayerEntity::MIDFIELDER:
                return array_splice($this->midfielders, 0, 1)[0] ?? null;
                break;
            case AbstractPlayerEntity::STRIKER:
                return array_splice($this->strikers, 0, 1)[0] ?? null;
                break;
            case AbstractPlayerEntity::GOALKEEPER:
                return array_splice($this->goalkeepers, 0, 1)[0] ?? null;
                break;
            default:
                throw new PlayerPositionNotExistException();
                break;
        }
    }
}