<?php

namespace App\League;

/**
 * Interface LeagueInterface
 * @package App\League
 */
interface LeagueInterface
{
    public function prepareTeams();

    public function play();

    public function getStatistics();
}