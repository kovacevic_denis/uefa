<?php

namespace App\League;

use App\Entity\Game\GameEntity;
use App\Service\GameService;
use App\Service\TeamService;

/**
 * Class UefaLeague
 * @package App\League
 */
class UefaLeague implements LeagueInterface
{
    /** @var TeamService */
    protected $teamService;

    /** @var  GameService */
    protected $gameService;

    protected $opponents;

    protected $myTeam;

    /** @var  GameEntity[] */
    protected $games;

    public function __construct(TeamService $teamService, GameService $gameService)
    {
        $this->teamService = $teamService;
        $this->gameService = $gameService;
    }

    public function prepareTeams()
    {
        $opponentsLevel = $this->gameService->getOpponentsLevel();

        foreach ($opponentsLevel as $level) {
            $this->opponents[] = $this->teamService->buildTeam($level);
        }

        $this->myTeam = $this->teamService->buildTeam($this->gameService->getMyTeamLevel());
    }

    public function play()
    {
        $this->prepareTeams();

        $this->games = $this->gameService->playGames($this->opponents, $this->myTeam);
    }

    public function getStatistics()
    {
        foreach ($this->games as $gameEntity){
            echo $gameEntity->getHome()->getLevel() . '-' . $gameEntity->getOpponent()->getLevel() . "\n";
            echo $gameEntity->getResult() . "\n";
        }
    }
}