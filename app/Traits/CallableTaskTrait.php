<?php

namespace App\Traits;

use App\Task\AbstractTask;


trait CallableTaskTrait
{
    /**
     * @param $class
     * @param array $runArguments
     */
    public function call($class, $runArguments = [])
    {
        $task = $this->container->get($class);

        if ($task instanceof AbstractTask) {
            return $task->run(...$runArguments);
        }
    }
}
