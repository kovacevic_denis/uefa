<?php
use App\Entity\Player\AbstractPlayerEntity;

if (!function_exists('orderPlayersByQuality')) {
    function orderPlayersByQuality(AbstractPlayerEntity $a, AbstractPlayerEntity $b)
    {
        if ($a->getQuality() > $b->getQuality()) {
            return -1;
        } elseif ($a->getQuality() < $b->getQuality()) {
            return 1;
        } elseif ($a->getSpeed() > $b->getSpeed()) {
            return -1;
        } elseif ($a->getSpeed() < $b->getSpeed()) {
            return 1;
        } else {
            return 0;
        }
    }
}

if (!function_exists('orderPlayersBySpeed')) {
    function orderPlayersBySpeed(AbstractPlayerEntity $a, AbstractPlayerEntity $b)
    {
        if ($a->getSpeed() > $b->getSpeed()) {
            return -1;
        } elseif ($a->getSpeed() < $b->getSpeed()) {
            return 1;
        } elseif ($a->getQuality() > $b->getQuality()) {
            return -1;
        } elseif ($a->getQuality() < $b->getQuality()) {
            return 1;
        } else {
            return 0;
        }
    }
}