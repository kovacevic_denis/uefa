<?php

namespace App\Service;

use App\Entity\Player\AbstractPlayerEntity;
use App\Entity\Team\AbstractTeamEntity;
use App\Entity\Team\FullTeamEntity;
use App\Task\Player\BuildRandomPlayerTask;
use App\Task\Team\BuildTeamTask;
use App\Task\Team\SetStarterTeamTask;

/**
 * Class TeamService
 * @package App\Service
 */
class TeamService extends AbstractService
{

    /**
     * @param $level
     * @return AbstractTeamEntity
     */
    public function buildTeam($level): AbstractTeamEntity
    {
        $playerPositions = $this->getPlayerPositions();
        $playerAbilities = $this->getPlayerAbilitiesByLevel($level);


        $players = [];
        foreach ($playerPositions as $position => $numberOfPlayers) {
            $players[$position] = $this->buildTeamPlayers(
                $playerAbilities,
                $position,
                $numberOfPlayers
            );
        }

        /** @var AbstractTeamEntity $team */
        $team = $this->call(BuildTeamTask::class, [$players]);

        $team = $this->call(SetStarterTeamTask::class, [$this->getDefaultFormation(), $team]);

        return $team;
    }

    /**
     * @return array
     */
    public function getPlayerPositions(): array
    {
        return $this->getConfig('team')['playerPositions'];
    }

    /**
     * @return array
     */
    private function getDefaultFormation(): array
    {
        return $this->getConfig('team')['defaultFormation'];
    }

    /**
     * @param array $playerAbilities
     * @param string $position
     * @param int $numberOfPlayers
     * @return AbstractPlayerEntity[]
     */
    protected function buildTeamPlayers(array $playerAbilities, string $position, int $numberOfPlayers): array
    {
        $players = [];
        for ($j = 0; $j < $numberOfPlayers; $j++) {
            $players[] = $this->call(BuildRandomPlayerTask::class, [
                $playerAbilities,
                $position
            ]);
        }

        return $players;
    }

    public function getPlayerAbilitiesByLevel($level = '')
    {
        switch ($level) {
            case AbstractTeamEntity::WEAK:
                $config = $this->getConfig('player')['weakPlayerAbilities'];
                break;
            case AbstractTeamEntity::STRONG:
                $config = $this->getConfig('player')['strongPlayerAbilities'];
                break;
            case AbstractTeamEntity::MIDDLE:
                $config = $this->getConfig('player')['middlePlayerAbilities'];
                break;
            default:
                $config = $this->getConfig('player')['defaultPlayerAbilities'];
                break;
        }

        return $config;
    }
}