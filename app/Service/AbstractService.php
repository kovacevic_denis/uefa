<?php

namespace App\Service;

use App\Traits\CallableTaskTrait;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class AbstractService
 * @package App\Service
 */
abstract class AbstractService
{
    use CallableTaskTrait;

    /** @var  Container */
    protected $container;

    public function setContainer(Container $container){
        $this->container = $container;
    }

    /**
     * @param string $configName
     * @return mixed
     */
    public function getConfig(string $configName)
    {
        return $this->container->getParameter($configName);
    }
}