<?php

namespace App\Service;

use App\Entity\Game\GameEntity;
use App\Entity\Team\FullTeamEntity;
use App\Task\Game\PlayGameTask;
use App\Task\Team\ReplaceInjuredPlayerTask;
use App\Task\Team\ResetStarterTeamTask;
use App\Task\Team\GetOpponentTeamLevelTask;
use App\Task\Team\RemoveRandomTeamPlayerInjuredTask;
use App\Task\Team\SetStarterTeamTask;

/**
 * Class TeamService
 * @package App\Service
 */
class GameService extends AbstractService
{

    public $results = [];

    /**
     * @param array $opponents
     * @param FullTeamEntity $myTeam
     * @return array
     */
    public function playGames(array $opponents, FullTeamEntity $myTeam)
    {
        $games = [];

        foreach ($opponents as $opponent) {
            $game = new GameEntity();
            $game->setOpponent($opponent);
            $game->setHome($myTeam);

            $games [] = $this->playGame($game);

            if (count($games) < count($opponents)) {
                $myTeam = $this->removeInjuredPlayer($myTeam);
            }
        }

        return $games;
    }

    public function playGame(GameEntity $gameEntity)
    {
        $level = $this->call(GetOpponentTeamLevelTask::class, [
            $gameEntity->getOpponent()->getStarterTeamEntity(),
            $gameEntity->getHome()->getStarterTeamEntity(),
        ]);

        $strategy = $this->getStrategyByLevel($level);

        $myTeam = $gameEntity->getHome();
        $myTeam = $this->call(ResetStarterTeamTask::class, [$myTeam]);
        $myTeam = $this->call(SetStarterTeamTask::class, [$strategy, $myTeam]);

        $gameEntity->setHome($myTeam);
        $result = $this->call(PlayGameTask::class, [$gameEntity->getOpponent(), $gameEntity->getHome()]);

        $gameEntity->setResult($result);

        return $gameEntity;
    }

    /**
     * @return array
     */
    public function getOpponentsLevel(): array
    {
        return $this->getConfig('game')['opponents'];
    }

    /**
     * @return string
     */
    public function getMyTeamLevel(): string
    {
        return $this->getConfig('game')['myTeam'];
    }

    /**
     * @param string $level
     * @return array
     */
    public function getStrategyByLevel(string $level): array
    {
        return $this->getConfig('strategies')[$level];
    }

    public function getReplacementCascadeConfig(): array
    {
        return $this->getConfig('player')['replacementCascade'];
    }

    protected function removeInjuredPlayer($myTeam)
    {
        /** @var FullTeamEntity $myTeam */
        $myTeam = $this->call(RemoveRandomTeamPlayerInjuredTask::class, [$myTeam]);
        $injuredPlayers = $myTeam->getInjuredPlayers();

        $lastInjuredPlayer = end($injuredPlayers);
        return $this->call(ReplaceInjuredPlayerTask::class, [
            $myTeam,
            $lastInjuredPlayer,
            $this->getReplacementCascadeConfig()
        ]);
    }
}