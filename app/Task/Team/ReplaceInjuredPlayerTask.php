<?php

namespace App\Task\Team;

use App\Entity\Player\AbstractPlayerEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class ReplaceInjuredPlayerTask
 * @package App\Task\Team
 */
class ReplaceInjuredPlayerTask extends AbstractTask
{
    /**
     * @param FullTeamEntity $fullTeamEntity
     * @param AbstractPlayerEntity $injuredPlayer
     * @param array $replacementCascade
     * @return FullTeamEntity
     */
    function run(FullTeamEntity $fullTeamEntity, AbstractPlayerEntity $injuredPlayer, array $replacementCascade)
    {
        $replacementCascadePosition = array_search($injuredPlayer->getPosition(), $replacementCascade);

        do {
            /** @var AbstractPlayerEntity $replacement */
            $replacement = $fullTeamEntity->spliceTopPlayerByPosition($replacementCascade[$replacementCascadePosition]);

            if ($replacement && $replacement->getPosition() !== $injuredPlayer->getPosition()) {
                $injuredPlayer->setQuality($replacement->getQuality());
                $injuredPlayer->setSpeed($replacement->getSpeed());
                $replacement = $injuredPlayer;
            }

            $replacementCascadePosition++;
        } while (is_null($replacement));

        $starterTeam = $fullTeamEntity->getStarterTeamEntity();
        $starterTeam->addPlayer($replacement);

        $fullTeamEntity->setStarterTeamEntity($starterTeam);

        return $fullTeamEntity;
    }
}