<?php

namespace App\Task\Team;

use App\Entity\Team\AbstractTeamEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class BuildTeamTask
 * @package App\Task\Team
 */
class SetStarterTeamTask extends AbstractTask
{
    /**
     * @param array $formation
     * @param FullTeamEntity $fullTeamEntity
     * @return FullTeamEntity
     */
    function run(array $formation, FullTeamEntity $fullTeamEntity)
    {
        foreach ($formation['orders'] as $position => $orderBy) {
            $fullTeamEntity->playersOrderByPosition($position, $orderBy);
        }

        return $this->setFormationByName($formation['name'], $fullTeamEntity);
    }

    /**
     * @param string $formationName
     * @param FullTeamEntity $fullTeamEntity
     * @return FullTeamEntity
     */
    protected function setFormationByName(string $formationName, FullTeamEntity $fullTeamEntity)
    {
        list($defenderCount, $midfielderCount, $strikerCount) = explode('-', $formationName);

        /** @var  AbstractTeamEntity $starterTeam */
        list($goalkeepers, $defenders, $midfielders, $strikers) = [
            $fullTeamEntity->getGoalkeepers(),
            $fullTeamEntity->getDefenders(),
            $fullTeamEntity->getMidfielders(),
            $fullTeamEntity->getStrikers(),
        ];

        $starterTeam = new StarterTeamEntity();
        $starterTeam->setGoalkeepers(array_splice($goalkeepers, 0, 1));
        $starterTeam->setDefenders(array_splice($defenders, 0, $defenderCount));
        $starterTeam->setMidfielders(array_splice($midfielders, 0, $midfielderCount));
        $starterTeam->setStrikers(array_splice($strikers, 0, $strikerCount));

        $fullTeamEntity->setStarterTeamEntity($starterTeam);
        $fullTeamEntity->setGoalkeepers($goalkeepers);
        $fullTeamEntity->setDefenders($defenders);
        $fullTeamEntity->setMidfielders($midfielders);
        $fullTeamEntity->setStrikers($strikers);

        return $fullTeamEntity;
    }
}