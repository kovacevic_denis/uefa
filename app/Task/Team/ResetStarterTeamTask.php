<?php

namespace App\Task\Team;

use App\Entity\Team\AbstractTeamEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class ResetStarterTeamTask
 * @package App\Task\Team
 */
class ResetStarterTeamTask extends AbstractTask
{
    /**
     * @param FullTeamEntity $fullTeamEntity
     * @return FullTeamEntity
     */
    function run(FullTeamEntity $fullTeamEntity)
    {
        $starterTeam = new StarterTeamEntity();
        $fullTeamEntity->setGoalkeepers(array_merge(
            $fullTeamEntity->getGoalkeepers(),
            $fullTeamEntity->getStarterTeamEntity()->getGoalkeepers()
        ));

        $fullTeamEntity->setDefenders(array_merge(
            $fullTeamEntity->getDefenders(),
            $fullTeamEntity->getStarterTeamEntity()->getDefenders()
        ));

        $fullTeamEntity->setMidfielders(array_merge(
            $fullTeamEntity->getMidfielders(),
            $fullTeamEntity->getStarterTeamEntity()->getMidfielders()
        ));

        $fullTeamEntity->setStrikers(array_merge(
            $fullTeamEntity->getStrikers(),
            $fullTeamEntity->getStarterTeamEntity()->getStrikers()
        ));

        $fullTeamEntity->setStarterTeamEntity($starterTeam);

        return $fullTeamEntity;
    }
}