<?php

namespace App\Task\Team;

use App\Entity\Team\AbstractTeamEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class BuildTeamTask
 * @package App\Task\Team
 */
class GetOpponentTeamLevelTask extends AbstractTask
{
    /**
     * @param StarterTeamEntity $opponent
     * @param StarterTeamEntity $home
     * @return FullTeamEntity
     */
    function run(StarterTeamEntity $opponent, StarterTeamEntity $home)
    {
        $level = AbstractTeamEntity::EQUAL;
        if($opponent->getLevel() === $home->getLevel()) {
            return $level;
        }

        if($home->getScore() > $opponent->getScore()) {
            $level = AbstractTeamEntity::STRONG;
        } elseif($home->getScore() < $opponent->getScore()) {
            $level = AbstractTeamEntity::WEAK;
        }

        return $level;
    }
}