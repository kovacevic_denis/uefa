<?php

namespace App\Task\Team;

use App\Entity\Player\AbstractPlayerEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class BuildTeamTask
 * @package App\Task\Team
 */
class RemoveRandomTeamPlayerInjuredTask extends AbstractTask
{
    /**
     * @param FullTeamEntity $fullTeamEntity
     * @return FullTeamEntity
     */
    function run(FullTeamEntity $fullTeamEntity)
    {
        $starterTeamAllPlayers = $fullTeamEntity->getStarterTeamEntity()->getAllPlayers();
        $randomPlayerKey = $this->getRandomPlayerNumber();

        /** @var AbstractPlayerEntity $player */
        $injuredPlayer = $starterTeamAllPlayers[$randomPlayerKey];
        unset($starterTeamAllPlayers[$randomPlayerKey]);

        $starterTeam = new StarterTeamEntity();
        $starterTeam->addPlayers($starterTeamAllPlayers);

        $fullTeamEntity->setStarterTeamEntity($starterTeam);

        $fullTeamEntity->addInjuredPlayer($injuredPlayer);

        return $fullTeamEntity;
    }

    /**
     * @return int
     */
    protected function getRandomPlayerNumber(): int
    {
        return rand(0, 10);
    }
}