<?php

namespace App\Task\Team;

use App\Entity\Team\FullTeamEntity;
use App\Task\AbstractTask;

/**
 * Class BuildTeamTask
 * @package App\Task\Team
 */
class BuildTeamTask extends AbstractTask
{
    /**
     * @param array $players
     * @return FullTeamEntity
     */
    function run(array $players)
    {
        $teamEntity = new FullTeamEntity();
        $teamEntity->addPlayersByPosition($players);

        return $teamEntity;
    }
}