<?php

namespace App\Task\Player;

use App\Entity\Player\AbstractPlayerEntity;
use App\Exceptions\PlayerPositionNotExistException;
use App\Task\AbstractTask;

/**
 * Class BuildPlayerTask
 * @package App\Task\Player
 */
class BuildRandomPlayerTask extends AbstractTask
{
    /**
     * @param array $playerAbilities
     * @param string $position
     * @return AbstractPlayerEntity
     * @throws PlayerPositionNotExistException
     */
    public function run(array $playerAbilities, string $position): AbstractPlayerEntity
    {
        $speed = $this->getRandom($playerAbilities['speedRange']);
        $quality = $this->getRandom($playerAbilities['qualityRange']);

        if (class_exists($position)) {
            return new $position($quality, $speed);
        }

        throw new PlayerPositionNotExistException();
    }

    /**
     * @param array $range
     * @return int
     */
    protected function getRandom(array $range): int
    {
        return rand($range['min'], $range['max']);
    }
}