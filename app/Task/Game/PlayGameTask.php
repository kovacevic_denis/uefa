<?php

namespace App\Task\Game;

use App\Entity\Game\GameEntity;
use App\Entity\Team\AbstractTeamEntity;
use App\Entity\Team\FullTeamEntity;
use App\Entity\Team\StarterTeamEntity;
use App\Task\AbstractTask;

/**
 * Class PlayGameTask
 * @package App\Task\Game
 */
class PlayGameTask extends AbstractTask
{
    const ROUNDS = 3;

    public function run(FullTeamEntity $opponent, FullTeamEntity $home)
    {
        $opponent = $opponent->getStarterTeamEntity();
        $home = $home->getStarterTeamEntity();

        $homeGoals = 0;
        $opponentGoals = 0;

        for ($i = 0; $i < 3; $i++) {
            $opponentScore = $opponent->getScore() + $this->getSurpriseFactor($opponent->getLevel());
            $homeScore = $home->getScore() + $this->getSurpriseFactor($home->getLevel());

            if ($opponentScore > $homeScore) {
                $opponentGoals++;
            } elseIf ($homeScore > $opponentScore) {
                $homeGoals++;
            }
        }

        return implode('-', [
            $homeGoals,
            $opponentGoals
        ]);
    }

    protected function getSurpriseFactor($level)
    {
        if ($level === AbstractTeamEntity::WEAK) {
            $multipleBy = rand(1, 2);
        } else {
            $multipleBy = rand(-1, 1);
        }

        return rand(1, 15) * $multipleBy;
    }
}