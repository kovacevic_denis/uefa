<?php
namespace App\Exceptions;

use Exception;

/**
 * Class AbstractException
 * @package App\Exceptions
 */
abstract class AbstractException extends Exception
{
}