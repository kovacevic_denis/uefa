<?php

namespace App\Exceptions;


class TeamNameNotExistException extends AbstractException
{
    protected $message = 'Team Name Not Exist';
}