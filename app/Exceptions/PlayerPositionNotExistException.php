<?php

namespace App\Exceptions;


class PlayerPositionNotExistException extends AbstractException
{
    protected $message = 'Player Position Not Exist';
}