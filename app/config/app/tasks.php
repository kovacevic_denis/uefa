<?php
/** register DI */

use App\Task\Game\PlayGameTask;
use App\Task\Player\BuildRandomPlayerTask;
use App\Task\Team\ReplaceInjuredPlayerTask;
use App\Task\Team\ResetStarterTeamTask;
use App\Task\Team\BuildTeamTask;
use App\Task\Team\GetOpponentTeamLevelTask;
use App\Task\Team\RemoveRandomTeamPlayerInjuredTask;
use App\Task\Team\SetStarterTeamTask;

$container
    ->register(BuildTeamTask::class, BuildTeamTask::class);
$container
    ->register(SetStarterTeamTask::class, SetStarterTeamTask::class);
$container
    ->register(GetOpponentTeamLevelTask::class, GetOpponentTeamLevelTask::class);
$container
    ->register(RemoveRandomTeamPlayerInjuredTask::class, RemoveRandomTeamPlayerInjuredTask::class);
$container
    ->register(ResetStarterTeamTask::class, ResetStarterTeamTask::class);
$container
    ->register(ReplaceInjuredPlayerTask::class, ReplaceInjuredPlayerTask::class);

$container
    ->register(BuildRandomPlayerTask::class, BuildRandomPlayerTask::class);

$container
    ->register(PlayGameTask::class, PlayGameTask::class);