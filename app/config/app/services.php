<?php
/** register DI */

use App\App;
use App\League\UefaLeague;
use App\Service\AbstractService;
use App\Service\GameService;
use App\Service\TeamService;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\Reference;

$container->register(AbstractService::class)
    ->setAbstract(true);

$definition = new ChildDefinition(AbstractService::class);
$definition->setClass(TeamService::class);
$container->setDefinition(TeamService::class, $definition);

$container
    ->register('teamService', TeamService::class)
    ->addMethodCall('setContainer', array($container));

$container
    ->register('gameService', GameService::class)
    ->addMethodCall('setContainer', array($container));

$container
    ->register('uefaLeague', UefaLeague::class)
    ->addArgument(new Reference('teamService'))
    ->addArgument(new Reference('gameService'));

$container
    ->register('app', App::class)
    ->addArgument($container)
    ->addArgument(new Reference('uefaLeague'));