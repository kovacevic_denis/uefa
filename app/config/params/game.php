<?php

use App\Entity\Team\AbstractTeamEntity;

$container->setParameter('game', [
    'opponents' => [
        AbstractTeamEntity::WEAK,
        AbstractTeamEntity::MIDDLE,
        AbstractTeamEntity::STRONG
    ],
    'myTeam' => AbstractTeamEntity::MIDDLE
]);
