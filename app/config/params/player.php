<?php

use App\Entity\Player\AbstractPlayerEntity;

$container->setParameter('player', [
    'defaultPlayerAbilities' => [
        'speedRange' => [
            'min' => AbstractPlayerEntity::MIN,
            'max' => AbstractPlayerEntity::MAX,
        ],

        'qualityRange' => [
            'min' => AbstractPlayerEntity::MIN,
            'max' => AbstractPlayerEntity::MAX,
        ]
    ],

    'strongPlayerAbilities' => [
        'speedRange' => [
            'min' => AbstractPlayerEntity::MIN_STRONG,
            'max' => AbstractPlayerEntity::MAX,
        ],

        'qualityRange' => [
            'min' => AbstractPlayerEntity::MIN_STRONG,
            'max' => AbstractPlayerEntity::MAX,
        ]
    ],

    'middlePlayerAbilities' => [
        'speedRange' => [
            'min' => AbstractPlayerEntity::MIN_MIDDLE,
            'max' => AbstractPlayerEntity::MAX_MIDDLE,
        ],

        'qualityRange' => [
            'min' => AbstractPlayerEntity::MIN_MIDDLE,
            'max' => AbstractPlayerEntity::MAX_MIDDLE,
        ]
    ],

    'weakPlayerAbilities' => [
        'speedRange' => [
            'min' => AbstractPlayerEntity::MIN,
            'max' => AbstractPlayerEntity::MAX_WEAK,
        ],

        'qualityRange' => [
            'min' => AbstractPlayerEntity::MIN,
            'max' => AbstractPlayerEntity::MAX_WEAK,
        ]
    ],
    'replacementCascade' => [
        AbstractPlayerEntity::GOALKEEPER,
        AbstractPlayerEntity::DEFENDER,
        AbstractPlayerEntity::MIDFIELDER,
        AbstractPlayerEntity::STRIKER,
    ]
]);
