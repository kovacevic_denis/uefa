<?php

use App\Entity\Player\AbstractPlayerEntity;
use App\Entity\Team\AbstractTeamEntity;

$container->setParameter('strategies', [
    AbstractTeamEntity::STRONG => [
        'name' => '5-4-1',
        'orders' => [
            AbstractPlayerEntity::DEFENDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::MIDFIELDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::STRIKER => 'orderPlayersBySpeed',
        ],
    ],

    AbstractTeamEntity::EQUAL => [
        'name' => '4-4-2',
        'orders' => [
            AbstractPlayerEntity::DEFENDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::MIDFIELDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::STRIKER => 'orderPlayersBySpeed',
        ],
    ],

    AbstractTeamEntity::WEAK => [
        'name' => '3-4-3',
        'orders' => [
            AbstractPlayerEntity::DEFENDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::MIDFIELDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::STRIKER => 'orderPlayersByQuality',
        ],
    ]
]);
