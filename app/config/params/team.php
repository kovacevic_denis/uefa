<?php

use App\Entity\Player\AbstractPlayerEntity;

$container->setParameter('team', [
    'playerPositions' => [
        \App\Entity\Player\GoalkeeperEntity::class => 2,
        \App\Entity\Player\DefenderEntity::class => 6,
        \App\Entity\Player\MidfielderEntity::class => 10,
        \App\Entity\Player\StrikerEntity::class => 4
    ],

    'defaultFormation' => [
        'name' => '4-4-2',
        'orders' => [
            AbstractPlayerEntity::DEFENDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::MIDFIELDER => 'orderPlayersByQuality',
            AbstractPlayerEntity::STRIKER => 'orderPlayersByQuality',
        ],
    ],
]);
