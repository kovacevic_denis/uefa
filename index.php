<?php
use App\App;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\PhpFileLoader;

require_once __DIR__ . '/vendor/autoload.php';

$container = new ContainerBuilder();

$loader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/app/config/app'));
$loader->load('services.php');
$loader->load('tasks.php');

$loader = new PhpFileLoader($container, new FileLocator(__DIR__ . '/app/config/params'));
$loader->load('game.php');
$loader->load('player.php');
$loader->load('strategies.php');
$loader->load('team.php');

/** @var App $app */
$app = $container->get('app');
$app->play();
$app->getResults();