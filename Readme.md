### Used Technologies
 - PHP7.1
 
### Used Libraries:
- "symfony/dependency-injection": "^4.0",
- "symfony/config": "^4.0",

### Installation, console run:
- git clone git@bitbucket.org:kovacevic_denis/uefa.git
- cd uefa
- composer install
- php index.php